import os
import sys
import ssl
import re
import json
from seleniumwire import webdriver
import polling2
import yaml
from yaml.loader import SafeLoader
import time
from webdriver_manager.chrome import ChromeDriverManager

def inputFunction(xpath, value,driver):
    element = polling2.poll(lambda: driver.find_element_by_xpath(xpath), step=0.5, timeout=7)
    element.send_keys(value)

def clickFunction(xpath,driver):
    clickElement = polling2.poll(lambda: driver.find_element_by_xpath(xpath), step=0.5, timeout=7)
    clickElement.click()
    time.sleep(10)

def seleniumWireLogin(yaml_file_name, login_endpoint,endpoint):
    # Create a new instance of the Chrome driver
    with open('YAML/{}'.format(yaml_file_name)) as f:
        data = yaml.load(f, Loader=SafeLoader)
    # chromedrivermangere manages the latest version of chromdriver
    driver = webdriver.Chrome(ChromeDriverManager().install())
    # driver = webdriver.Chrome()

    driver.get(login_endpoint)
    # window size can we updated according to need
    driver.set_window_size(1200, 900)
    time.sleep(6)
    print('<-----------------------------YAML file {} execution started!!!'.format(yaml_file_name),'--------------------->\n')
    for i in range(len(data['action'])):
        if data['action'][i] == 'input':
            inputFunction(data['xpath'][i],data['values'][i],driver)
        if data['action'][i] == 'click':
            clickFunction(data['xpath'][i],driver)
            
    # Access requests via the `requests` attribute
    print('<--------------------------------YAML file execution complete!!!-------------------------------->\n')
    # once yaml execution is complete we find token from final request
    for request in driver.requests:
        if request.response:
            if str(request.url) == endpoint:
                print('Access Request Object Here')

seleniumWireLogin('login_info.yaml', 'https://mtxdev-vermont.cs32.force.com/UtilityArrearsBroadband/s/login/','https://mtxdev-vermont.cs32.force.com/UtilityArrearsBroadband/s/')